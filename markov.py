import random 
import os

def train():
    model = {}
    module_dir = os.path.dirname(__file__)
    file_path = os.path.join(module_dir, 'data_set.txt')
    with open(file_path, 'r') as dataset_file:
        for line in dataset_file:
            line = line.lower().split()
            for i, word in enumerate(line):
                if i == len(line)-1:   
                    model['END'] = model.get('END', []) + [word]
                else:    
                    if i == 0:
                        model['START'] = model.get('START', []) + [word]
                    model[word] = model.get(word, []) + [line[i+1]]
    return model

def generate():
    model = train()
    generated = []
    while True:
        if not generated:
            words = model['START']
        elif generated[-1] in model['END']:
            break
        else:
            words = model[generated[-1]]
        generated.append(random.choice(words))
    print(' '.join(generated))
        

generate()